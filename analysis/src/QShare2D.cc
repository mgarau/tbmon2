/* 
 * File:   QShare2D.cc
 * Author: daniel
 * 
 * Created on 7. März 2014, 14:28
 */

#include "QShare2D.h"

void QShare2D::init(const TBCore* core) 
{
	//xSharedPitch = 100.0;
	//ySharedPitch = 20.0;
	
	for(auto dut: core->usedDUT)
	{
		int iden = dut->iden;
		int maxToT = dut->getMaxTot();
		
		for(auto geometry: dut->pixelGeometries)
		{
			double maxPixelPitchX = geometry->getMaxPitchX();
			double maxPixelPitchY = geometry->getMaxPitchY();
			
			// Bins anpassen ?
			TH2D* anyHitTrkMap = new TH2D("", ";Track X [#mum];Track Y [#mum]", (int) (maxPixelPitchX)/2, -maxPixelPitchX*.5, maxPixelPitchX*1.5, (int) (maxPixelPitchY)/2, -maxPixelPitchY*.5, maxPixelPitchY*1.5);
			TH2D* sharedHitTrkMap = new TH2D("", ";Track X [#mum];Track Y [#mum]", (int) (maxPixelPitchX)/2, -maxPixelPitchX*.5, maxPixelPitchX*1.5, (int) (maxPixelPitchY)/2, -maxPixelPitchY*.5, maxPixelPitchY*1.5);
			TH2D* anyHitTrkMap_mirror = new TH2D("", ";Track X [#mum];Track Y [#mum]", (int) (maxPixelPitchX)/2, -maxPixelPitchX*.5, maxPixelPitchX*1.5, (int) (maxPixelPitchY)/2, -maxPixelPitchY*.5, maxPixelPitchY*1.5);
			TH2D* sharedHitTrkMap_mirror = new TH2D("", ";Track X [#mum];Track Y [#mum]", (int) (maxPixelPitchX)/2, -maxPixelPitchX*.5, maxPixelPitchX*1.5, (int) (maxPixelPitchY)/2, -maxPixelPitchY*.5, maxPixelPitchY*1.5);
			TH3D* directCharge = new TH3D("", ";Track X [#mum];Track Y [#mum];Charge in Hit Pixel", (int) (maxPixelPitchX)/2, -maxPixelPitchX*.5, maxPixelPitchX*1.5, (int) (maxPixelPitchY)/2, -maxPixelPitchY*.5, maxPixelPitchY*1.5, maxToT, 0, maxToT);
			TH3D* chargeShareScatter = new TH3D("", ";Track X [#mum];Track Y [#mum];Fraction of Charge in Hit Pixel", (int) (maxPixelPitchX)/2, -maxPixelPitchX*.5, maxPixelPitchX*1.5, (int) (maxPixelPitchY)/2, -maxPixelPitchY*.5, maxPixelPitchY*1.5, 200, 0.0, 1.0);
			
			QShare2D::h_anyHitTrkMap[iden].push_back(anyHitTrkMap);
			QShare2D::h_sharedHitTrkMap[iden].push_back(sharedHitTrkMap);
			QShare2D::h_anyHitTrkMap_mirror[iden].push_back(anyHitTrkMap_mirror);
			QShare2D::h_sharedHitTrkMap_mirror[iden].push_back(sharedHitTrkMap_mirror);
			QShare2D::h_directCharge[iden].push_back(directCharge);
			QShare2D::h_chargeShareScatter[iden].push_back(chargeShareScatter);

			QShare2D::numTracksPixel[iden].push_back(0);
			
			QShare2D::doCharge = false;
			// range is set up for ToT... change if Q needed
			if(QShare2D::doCharge == true)
			{
				QShare2D::h_directCharge[iden].back()->GetZaxis()->SetRangeUser(0.0, 60000.0);
			}
		}
		QShare2D::numTracks[iden] = 0;
	}	
}

void QShare2D::event(const TBCore* core, const TBEvent* event)
{
	DUT* dut = event->dut;
	int iden = event->iden;
	
	if(event->fTracks != kGood)
	{
		return;
	}
	
	if(event->fClusters != kGood)
	{
		return;
	}
	
	for(auto track: event->tracks)
	{
		if(track->fTrackMaskedRegion == kGood)
		{
			continue;
		}
		
		if(track->fMatchedCluster != kGood)
		{
			continue;
		}
		
		if(track->matchedCluster->hits.size() > 4 or track->matchedCluster->hits.size() == 0)
		{
			continue;
		}
		
		QShare2D::numTracks[iden]++;
		
		int trackCol = track->trackCol;
		int trackRow = track->trackRow;
		
		PixelGeometry* pixGeo = dut->pixelArray[trackCol][trackRow].getGeometry();
		int geometryNum = dut->getGeometryNumber(pixGeo);
		double maxPixelPitchX = pixGeo->getMaxPitchX();
		double maxPixelPitchY = pixGeo->getMaxPitchY();
		
		QShare2D::numTracksPixel[iden][geometryNum]++;
		
		double trackXMod = std::fmod(track->trackX, 2 * maxPixelPitchX) - maxPixelPitchX/2;
		double trackYMod = std::fmod(track->trackY, 2 * maxPixelPitchY) - maxPixelPitchY/2;
		
		double trackGeometricCenterX = dut->pixelArray[trackCol][trackRow].getGeometricCenterX();
		double trackGeometricCenterY = dut->pixelArray[trackCol][trackRow].getGeometricCenterY();
		
		// Loop over all hits in cluster
		bool hit_through = false;
		bool hit_vert = false;
		bool hit_hor = false;
		bool hit_diag = false;
		int hitCol = 0;
		int hitRow = 0;
		int hitThroughCol = 0;
		int hitThroughRow = 0;
		int hitThroughToT = 0;
		double hitGeometricCenterX;
		double hitGeometricCenterY;
		double directCharge;
		
		for(auto hit: track->matchedCluster->hits)
		{
			hitCol = hit->col;
			hitRow = hit->row;
			hitGeometricCenterX = dut->pixelArray[hitCol][hitRow].getGeometricCenterX();
			hitGeometricCenterY = dut->pixelArray[hitCol][hitRow].getGeometricCenterY();
			
			// only look at neighbour pixel
			if(hitCol < trackCol-1 or hitCol > trackCol+1 or hitRow < trackRow-1 or hitRow > trackRow+1)
			{
				continue;
			}
			
			// Pixel firing has track through it
			if(hitCol == trackCol and hitRow == trackRow) 
			{
				hit_through = true;
				hitThroughCol = hitCol;
				hitThroughRow = hitRow;
				hitThroughToT = hit->tot;
			}

			// Pixel firing has track above or below
			if(hitGeometricCenterX == trackGeometricCenterX and (hitGeometricCenterY < trackGeometricCenterY or hitGeometricCenterY > trackGeometricCenterY)) 
			{
				hit_vert = true;
			}

			// Pixel firing has track to the left or right
			if(hitGeometricCenterY == trackGeometricCenterY and (hitGeometricCenterX < trackGeometricCenterX or hitGeometricCenterX > trackGeometricCenterX))
			{
				hit_hor = true;
			}
			// Pixel firing has track diagonaly to it
			if((hitGeometricCenterX < trackGeometricCenterX or hitGeometricCenterX > trackGeometricCenterX) and (hitGeometricCenterY < trackGeometricCenterX or hitGeometricCenterX > trackGeometricCenterY)) 
			{
				hit_diag = true;
			}
		} // end for
		
		if(hit_through or hit_vert or hit_hor or hit_diag)
		{
			QShare2D::h_anyHitTrkMap[iden][geometryNum]->Fill(trackXMod, trackYMod);
			QShare2D::h_anyHitTrkMap_mirror[iden][geometryNum]->Fill(maxPixelPitchX - trackXMod, trackYMod);
		}

		if(hit_through and (hit_vert or hit_hor or hit_diag)) 
		{
			QShare2D::h_sharedHitTrkMap[iden][geometryNum]->Fill(trackXMod, trackYMod);
			QShare2D::h_sharedHitTrkMap_mirror[iden][geometryNum]->Fill(maxPixelPitchX - trackXMod, trackYMod);
		}
		
		
		if(hit_through) 
		{
			if(QShare2D::doCharge == true)
			{
				directCharge = (!event->dut->totcalib->hasCalib()) ? hitThroughToT : dut->q(hitThroughToT, hitThroughCol, hitThroughRow);
			} 
			else
			{
				directCharge = hitThroughToT;
			}
			
			QShare2D::h_directCharge[iden][geometryNum]->Fill(trackXMod, trackYMod, directCharge);

			if(QShare2D::doCharge == true)
			{
				if(hit_hor or hit_vert or hit_diag) 
				{
					QShare2D::h_chargeShareScatter[iden][geometryNum]->Fill(trackXMod, trackYMod, directCharge / TBCluster::getSumCharge(track->matchedCluster, event));
				}
			} 
			else 
			{
				if(hit_hor or hit_vert or hit_diag) 
				{
					QShare2D::h_chargeShareScatter[iden][geometryNum]->Fill(trackXMod, trackYMod, directCharge / TBCluster::getSumToT(track->matchedCluster));
				}
			}
		} 
		else 
		{
			QShare2D::h_chargeShareScatter[iden][geometryNum]->Fill(trackXMod, trackYMod, 0.0);
		}
	}
}

void QShare2D::finalize(const TBCore* core)
{
	core->output->processName = QShare2D::name;
	
	char* histoTitle = new char[500];
	for(auto dut: core->usedDUT)
	{
		int iden = dut->iden;
		
		// set up cuts
		core->output->cuts = "good matched tracks with matched cluster size <= 4";
		core->output->currentPreprocessCuts = "";
		// -----------
		
		for(auto geometry: dut->pixelGeometries)
		{
			int geometryNumber = dut->getGeometryNumber(geometry);
			
			std::string edge = "";
			if(dut->pixelGeometries[geometryNumber]->isEdgePixel() == true)
			{
				edge = "(Edge Pixel)";
			}
			
			// Get overall charge sharing probability
			double nAllHits = QShare2D::h_anyHitTrkMap[iden][geometryNumber]->GetEntries();
			double nSharedHits = QShare2D::h_sharedHitTrkMap[iden][geometryNumber]->GetEntries();

			TBLOG(kINFO, "DUT iden: " << iden << " Geometry: " << geometryNumber);
			TBLOG(kINFO, "Tracks:                 " << QShare2D::numTracksPixel[iden][geometryNumber]);
			TBLOG(kINFO, "All Hits:               " << nAllHits);
			TBLOG(kINFO, "Shared Hits:            " << nSharedHits);
			TBLOG(kINFO, "Overall Charge Sharing: " << nSharedHits / nAllHits);
			
			
			std::sprintf(histoTitle, "Any Hit Track Map DUT %i Geometry %i %s", iden, geometryNumber, edge.c_str());
			QShare2D::h_anyHitTrkMap[iden][geometryNumber]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "anyHitTrkMap_dut_%i_geometry_%i", iden, geometryNumber);
			QShare2D::h_anyHitTrkMap[iden][geometryNumber]->SetName(histoTitle);
			core->output->drawAndSave(QShare2D::h_anyHitTrkMap[iden][geometryNumber], "colz", "e");
			
			std::sprintf(histoTitle, "Shared Hit Track Map DUT %i Geometry %i %s", iden, geometryNumber, edge.c_str());
			QShare2D::h_sharedHitTrkMap[iden][geometryNumber]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "sharedHitTrkMap_dut_%i_geometry_%i", iden, geometryNumber);
			QShare2D::h_sharedHitTrkMap[iden][geometryNumber]->SetName(histoTitle);
			core->output->drawAndSave(QShare2D::h_sharedHitTrkMap[iden][geometryNumber],"colz", "e");
			
			std::sprintf(histoTitle, "Any Hit Track Map Mirror DUT %i Geometry %i %s", iden, geometryNumber, edge.c_str());
			QShare2D::h_anyHitTrkMap_mirror[iden][geometryNumber]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "anyHitTrkMap_mirror_dut_%i_geometry_%i", iden, geometryNumber);
			QShare2D::h_anyHitTrkMap_mirror[iden][geometryNumber]->SetName(histoTitle);
			core->output->drawAndSave(QShare2D::h_anyHitTrkMap_mirror[iden][geometryNumber], "colz", "e");
			
			std::sprintf(histoTitle, "Shared Hit Track Map Mirror DUT %i Geometry %i %s", iden, geometryNumber, edge.c_str());
			QShare2D::h_sharedHitTrkMap_mirror[iden][geometryNumber]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "sharedHitTrkMap_mirror_dut_%i_geometry_%i", iden, geometryNumber);
			QShare2D::h_sharedHitTrkMap_mirror[iden][geometryNumber]->SetName(histoTitle);
			core->output->drawAndSave(QShare2D::h_sharedHitTrkMap_mirror[iden][geometryNumber], "colz", "e");
			
			std::sprintf(histoTitle, "Direct Charge DUT %i Geometry %i %s", iden, geometryNumber, edge.c_str());
			QShare2D::h_directCharge[iden][geometryNumber]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "directCharge_dut_%i_geometry_%i", iden, geometryNumber);
			QShare2D::h_directCharge[iden][geometryNumber]->SetName(histoTitle);
			core->output->drawAndSave(QShare2D::h_directCharge[iden][geometryNumber], "", "e");
			
			std::sprintf(histoTitle, "Charge Share Scatter DUT %i Geometry %i %s", iden, geometryNumber, edge.c_str());
			QShare2D::h_chargeShareScatter[iden][geometryNumber]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "chargeShareScatter_dut_%i_geometry_%i", iden, geometryNumber);
			QShare2D::h_chargeShareScatter[iden][geometryNumber]->SetName(histoTitle);
			core->output->drawAndSave(QShare2D::h_chargeShareScatter[iden][geometryNumber], "", "e");
			
			std::sprintf(histoTitle, "Direct Charge Profile DUT %i Geometry %i %s", iden, geometryNumber, edge.c_str());
			QShare2D::h_directCharge[iden][geometryNumber]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "directChargeProfile_dut_%i_geometry_%i", iden, geometryNumber);
			QShare2D::h_directCharge[iden][geometryNumber]->SetName(histoTitle);
			core->output->drawAndSave(QShare2D::h_directCharge[iden][geometryNumber]->Project3DProfile(), "colz", "e");
			
			std::sprintf(histoTitle, "Charge Share Scatter Profile DUT %i Geometry %i %s", iden, geometryNumber, edge.c_str());
			QShare2D::h_chargeShareScatter[iden][geometryNumber]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "chargeShareScatterProfile_dut_%i_geometry_%i", iden, geometryNumber);
			QShare2D::h_chargeShareScatter[iden][geometryNumber]->SetName(histoTitle);
			core->output->drawAndSave(QShare2D::h_chargeShareScatter[iden][geometryNumber]->Project3DProfile(), "colz", "e");
			
			// TODO drawAspect?!
			
			QShare2D::h_sharedHitTrkMap[iden][geometryNumber]->Divide(QShare2D::h_anyHitTrkMap[iden][geometryNumber]);
			QShare2D::h_sharedHitTrkMap[iden][geometryNumber]->GetZaxis()->SetRangeUser(0, 1);
			//QShare2D::h_sharedHitTrkMap[iden][geometryNumber]->SetStats(kFALSE);
			
			std::sprintf(histoTitle, "Charge Share 2D DUT %i Geometry %i %s", iden, geometryNumber, edge.c_str());
			QShare2D::h_sharedHitTrkMap[iden][geometryNumber]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "chargeSharing2D_dut_%i_geometry_%i", iden, geometryNumber);
			QShare2D::h_sharedHitTrkMap[iden][geometryNumber]->SetName(histoTitle);
			core->output->drawAndSave(QShare2D::h_sharedHitTrkMap[iden][geometryNumber], "cont4z", "e");
			
			QShare2D::h_sharedHitTrkMap_mirror[iden][geometryNumber]->Divide(QShare2D::h_anyHitTrkMap_mirror[iden][geometryNumber]);
			QShare2D::h_sharedHitTrkMap_mirror[iden][geometryNumber]->GetZaxis()->SetRangeUser(0, 1);
			//QShare2D::h_sharedHitTrkMap_mirror[iden][geometryNumber]->SetStats(kFALSE);
			
			std::sprintf(histoTitle, "Charge Share 2D Mirror DUT %i Geometry %i %s", iden, geometryNumber, edge.c_str());
			QShare2D::h_sharedHitTrkMap_mirror[iden][geometryNumber]->SetTitle(histoTitle);
			std::sprintf(histoTitle, "chargeSharing2D_mirror_dut_%i_geometry_%i", iden, geometryNumber);
			QShare2D::h_sharedHitTrkMap_mirror[iden][geometryNumber]->SetName(histoTitle);
			core->output->drawAndSave(QShare2D::h_sharedHitTrkMap_mirror[iden][geometryNumber], "cont4z", "e");
			
			delete QShare2D::h_anyHitTrkMap[iden][geometryNumber];
			delete QShare2D::h_sharedHitTrkMap[iden][geometryNumber];
			delete QShare2D::h_anyHitTrkMap_mirror[iden][geometryNumber];
			delete QShare2D::h_sharedHitTrkMap_mirror[iden][geometryNumber];
			delete QShare2D::h_directCharge[iden][geometryNumber];
			delete QShare2D::h_chargeShareScatter[iden][geometryNumber];
		}
		
		QShare2D::h_anyHitTrkMap[iden].clear();
		QShare2D::h_sharedHitTrkMap[iden].clear();
		QShare2D::h_anyHitTrkMap_mirror[iden].clear();
		QShare2D::h_sharedHitTrkMap_mirror[iden].clear();
		QShare2D::h_directCharge[iden].clear();
		QShare2D::h_chargeShareScatter[iden].clear();
	}
	
	QShare2D::h_anyHitTrkMap.clear();
	QShare2D::h_sharedHitTrkMap.clear();
	QShare2D::h_anyHitTrkMap_mirror.clear();
	QShare2D::h_sharedHitTrkMap_mirror.clear();
	QShare2D::h_directCharge.clear();
	QShare2D::h_chargeShareScatter.clear();
	
	delete [] histoTitle;
}