/* 
 * File:   Lv1Cut.h
 * Author: daniel
 *
 * Created on 15. Februar 2014, 14:41
 */

#ifndef LV1CUT_H
#define	LV1CUT_H

#include "TBAnalysis.h"

class Lv1Cut: public TBAnalysis
{
private:
	std::map<IDEN, TH1I*> h_lvl1HistAny;
	std::map<IDEN, TH1I*> h_lvl1HistMatched;

public:
	Lv1Cut()
	{
		name = "Lv1Cut";
	}
	virtual void init(const TBCore* core);
	virtual void event(const TBCore* core, const TBEvent* event);
	virtual void finalize(const TBCore* core);

};

// class factories
extern "C" TBAnalysis* create()
{
	return new Lv1Cut;
}

extern "C" void destroy(TBAnalysis* tba)
{
	delete tba;
}

#endif	/* LV1CUT_H */

