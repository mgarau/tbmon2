/* 
 * File:   ClusterChecker.h
 * Author: daniel
 *
 * Created on 15. Februar 2014, 15:22
 */

#ifndef CLUSTERCHECKER_H
#define	CLUSTERCHECKER_H

#include "TBAnalysis.h"

class ClusterChecker: public TBAnalysis
{
private:
	std::map<IDEN, TH1D*> h_clusterMult;
	std::map<IDEN, TH1D*> h_nHits;
	std::map<IDEN, TH1D*> h_matchClusterSize;
	std::map<IDEN, TH1D*> h_matchClusterSizeX;
	std::map<IDEN, TH1D*> h_matchClusterSizeY;
	std::map<IDEN, TH1D*> h_unmatchClusterSize;
	std::map<IDEN, TH1D*> h_matchClusterLvl1;
	std::map<IDEN, TH2D*> h_matchClusterLvl1Map;
	std::map<IDEN, TH2D*> h_matchClusterToTCenterMap;

public:
	ClusterChecker()
	{
		TBAnalysis::name = "ClusterChecker";
	}
	virtual void init(const TBCore* core);
	virtual void event(const TBCore* core, const TBEvent* event);
	virtual void finalize(const TBCore* core);
};

// class factories
extern "C" TBAnalysis* create()
{
	return new ClusterChecker;
}

extern "C" void destroy(TBAnalysis* tba)
{
	delete tba;
}

#endif	/* CLUSTERCHECKER_H */

