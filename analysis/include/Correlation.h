/* 
 * File:   Correlation.h
 * Author: daniel
 *
 * Created on 6. März 2014, 15:09
 */

#ifndef CORRELATION_H
#define	CORRELATION_H

#include "TBAnalysis.h"


class Correlation: public TBAnalysis
{
private:
	std::map<IDEN, TH2D*> h_colvx;
	std::map<IDEN, TH2D*> h_colvy;
	std::map<IDEN, TH2D*> h_rowvx;
	std::map<IDEN, TH2D*> h_rowvy;

	std::map<IDEN, TH2D*> h_occup;

public:
	Correlation()
	{
		TBAnalysis::name = "Correlation";
	}
	virtual void init(const TBCore* core);
	virtual void event(const TBCore* core, const TBEvent* event);
	virtual void finalize(const TBCore* core);
};

// class factories
extern "C" TBAnalysis* create()
{
	return new Correlation;
}

extern "C" void destroy(TBAnalysis* tba)
{
	delete tba;
}

#endif	/* CORRELATION_H */

