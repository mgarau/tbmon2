/* 
 * File:   SumToT.h
 * Author: daniel
 *
 * Created on 24. Februar 2014, 19:37
 */

#ifndef SUMTOT_H
#define	SUMTOT_H

#include "TBAnalysis.h"

class SumToT: public TBAnalysis
{
private:
	std::map<IDEN, TH1D*> h_fullChipTot;
	std::map<IDEN, TH1D*> h_fullChipQ;
	std::map<IDEN, TH1D*> h_maxClusterTot;
	std::map<IDEN, TH1D*> h_maxClusterQ;
	std::map<IDEN, TH1D*> h_maxCellTot;
	std::map<IDEN, TH1D*> h_maxCellQ;
	std::map<IDEN, TH1D*> h_matchClusterTot;
	std::map<IDEN, TH1D*> h_matchClusterQ;
	std::map<IDEN, TH1D*> h_matchClusterQ_1;
	std::map<IDEN, TH1D*> h_matchClusterQ_2;
	std::map<IDEN, TH1D*> h_matchClusterQ_3;
	std::map<IDEN, TH1D*> h_matchClusterQ_4;
	std::map<IDEN, TH1D*> h_matchClusterQ_5plus;
	std::map<IDEN, TH1D*> h_clusterq_no_bg;

	std::map<IDEN, TH3D*> h_matchClusterTotMap;
	std::map<IDEN, TH3D*> h_matchClusterQMap;
	
	std::map<IDEN, std::vector<TH2D*> > h_elecMap;
	std::map<IDEN, std::vector<TH3D*> > h_elecMapTot;
	std::map<IDEN, std::vector<TH1D*> > h_elecDist;
	std::map<IDEN, std::vector<TH2D*> > h_elecDistTot;
	
	//std::map<IDEN, TH2D*> h_mapElec2;

	bool doCharge;
	
	void ElecDistToT(const TBCore* core, const char* histoTitle, const char* histoName, TH2D* elecDistTot);
	
public:
	SumToT()
	{
		TBAnalysis::name= "SumToT";
	}
	virtual void init(const TBCore* core);
	virtual void event(const TBCore* core, const TBEvent* event);
	virtual void finalize(const TBCore* core);
};

// class factories
extern "C" TBAnalysis* create()
{
	return new SumToT;
}

extern "C" void destroy(TBAnalysis* tba)
{
	delete tba;
}

#endif //SUMTOT_H

