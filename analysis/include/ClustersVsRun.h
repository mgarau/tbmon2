/* 
 * File:   ClusterVsRun.h
 * Author: daniel
 *
 * Created on 24. Februar 2014, 21:09
 */

#ifndef CLUSTERVSRUN_H
#define	CLUSTERVSRUN_H

#include "TBAnalysis.h"
/**
*  calculates and plots mean cluster size (total, in x and in y) for each run
*
*/
class ClustersVsRun : public TBAnalysis
{
private:
	/// histograms shows mean total cluster size for each run
	std::map<IDEN, TH1D*> histo_matchClusterSizeVsRun;
	/// histograms shows mean total cluster in x size for each run
	std::map<IDEN, TH1D*> histo_matchClusterSizeXVsRun;
	/// histograms shows mean total cluster size in y for each run
	std::map<IDEN, TH1D*> histo_matchClusterSizeYVsRun;
	
	///total number of clusters per run
	std::map<IDEN, std::map<int, int> > nclusters;	
	///sum of all cluster sizes per run
	std::map<IDEN, std::map<int, int> > sumclusters;	
	///sum of all cluster sizes in x per run
	std::map<IDEN, std::map<int, int> > sumclusters_x;	
	///sum of all cluster sizes in y per run
	std::map<IDEN, std::map<int, int> > sumclusters_y;	
	
	///maximum cluster size on output plots
	std::map<IDEN, double> ClusterMax;	
	///minimum cluster size on output plots
	std::map<IDEN, double> ClusterMin;	
	
	/// graph shows mean total cluster size for each run
	std::map<IDEN, TGraphErrors*> h_matchClusterSizeVsRun;
	/// graph shows mean total cluster in x size for each run
	std::map<IDEN, TGraphErrors*> h_matchClusterSizeXVsRun;
	/// graph shows mean total cluster size in y for each run	///
	std::map<IDEN, TGraphErrors*> h_matchClusterSizeYVsRun;


	void drawAndSave( const TBCore& core,
		    std::string analysisName,
		    const char* histoName,
		    TGraphErrors* gr, DUT* dut);

 public:
	ClustersVsRun()
	{
		TBAnalysis::name = "ClustersVsRun";
	}
	virtual void init(const TBCore* core);
	virtual void event(const TBCore* core, const TBEvent* event);
	virtual void finalize(const TBCore* core);
};

// class factories
extern "C" TBAnalysis* create()
{
	return new ClustersVsRun;
}

extern "C" void destroy(TBAnalysis* tba)
{
	delete tba;
}

#endif	/* CLUSTERVSRUN_H */

