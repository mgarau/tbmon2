#ifndef TOTANA_H
#define TOTANA_H

#include "TBAnalysis.h"

class ToTAna: public TBAnalysis
{
private:
	std::map<IDEN, int> nhits;
	std::map<IDEN, double> meanToT;

	std::map<IDEN, std::vector<std::vector<int> > > hitToT;

	std::map<IDEN, TH2D*> h_ToTHeatMap;
	std::map<IDEN, TH2D*> h_ToTHeatNormMap;
	std::map<IDEN, TH2D*> h_ToTSigmaMap;
	std::map<IDEN, TH2D*> h_ToTDiffMap;
	
public:
	ToTAna()
	{
		TBAnalysis::name= "ToTAna";
	}
	virtual void init(const TBCore* core);
	virtual void event(const TBCore* core, const TBEvent* event);
	virtual void finalize(const TBCore* core);
};

// class factories
extern "C" TBAnalysis* create()
{
	return new ToTAna;
}

extern "C" void destroy(TBAnalysis* tba)
{
	delete tba;
}

#endif //TOTANA_H

